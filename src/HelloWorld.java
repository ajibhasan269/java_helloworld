public class HelloWorld {

    public static void main(String[] args) {
        // Prints "Hello, World" to the terminal window.
        System.out.println("Hello, World");
    }

}

// Install Jdk, https://www.oracle.com/ca-en/java/technologies/javase-jdk11-downloads.html

// mkdir Desktop/src
// mkdir Desktop/class

// cd Desktop

// Compile 
// javac -cp class -d class src/HelloWorld.java

// Execute
// java -cp class HelloWorld

// └── Java_HelloWorld
//     ├── class
//     │   └── HelloWorld.class
//     └── src
//         └── HelloWorld.java
